### Generate TikZ diagrams in Python

This python script generates TikZ code for diagrammatic representations of Wick contractions between different vertices.

License: GPL (version 2 or higher)
